import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import ContactFormContainer from "./containers/ContactFormContainer/index";

ReactDOM.render(
  <React.StrictMode>
    <ContactFormContainer />
  </React.StrictMode>,
  document.getElementById("root"),
);
