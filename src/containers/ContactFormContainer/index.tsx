import React, { Fragment, useState } from "react";
import ContactForm from "../../components/ContactForm/index";
import FetchMsg from "../../components/FetchMsg/index";
import ToggleAPI from "../../components/ToggleAPI/index";

export interface FetchStatus {
  status: "" | "loading" | "success" | "error";
  errorCode: undefined | number;
}

type ChangeElement =
  | React.ChangeEvent<HTMLInputElement>
  | React.ChangeEvent<HTMLTextAreaElement>;

export interface HandleSubmit {
  (e: React.FormEvent<HTMLFormElement>): void;
}

export interface HandleChange {
  (e: ChangeElement): void;
}

export interface FormState {
  name: string;
  email: string;
  gender: string;
  message: string;
}

const initFormState = (): FormState => {
  return {
    name: "",
    email: "",
    gender: "",
    message: "",
  };
};

// API endpoints for form submission
const APIs = {
  success: "https://httpbin.org/delay/3",
  failure: "https://httpbin.org/status/400",
};

const ContactFormContainer: React.FC = () => {
  const [formState, setFormState] = useState(initFormState());
  const [fetchStatus, setFetchStatus]: [FetchStatus, any] = useState({
    status: "",
    errorCode: undefined,
  });
  const [fetchWillSucceed, setFetchWillSucceed] = useState(true);

  // Form submission
  const handleSubmit: HandleSubmit = e => {
    if (e) e.preventDefault();
    console.log("form state:\n", formState);
    // get the spinner spinning
    setFetchStatus({ status: "loading", error: {} });
    let theAPI: string;
    if (fetchWillSucceed) theAPI = APIs.success;
    else theAPI = APIs.failure;
    // POST to API
    fetch(theAPI, {
      method: "POST",
      body: JSON.stringify(formState),
      headers: { "Content-Type": "application/json" },
    })
      .then(res => {
        if (res.status === 200) {
          // success: update banner & clear form
          setFetchStatus({ status: "success", errorCode: undefined });
          setFormState(initFormState());
          // error: update banner (keep form state!)
        } else setFetchStatus({ status: "error", errorCode: res.status });
      })
      .catch(err => {
        console.error(err);
        setFetchStatus({ status: "error", errorCode: -1 });
      });
  };

  // Form change by user
  const handleChange: HandleChange = e => {
    if (e) e.persist();
    setFormState({ ...formState, [e.target.name]: e.target.value });
  };

  // Toggle working/broken API
  const toggleAPI = (): void => {
    setFetchWillSucceed(!fetchWillSucceed);
  };

  return (
    <Fragment>
      {/* The actual form */}
      <ContactForm
        formState={formState}
        handleSubmit={handleSubmit}
        handleChange={handleChange}
      />
      {/* API status banner */}
      <FetchMsg fetchStatus={fetchStatus} />
      {/* DEV: Control API Success */}
      <ToggleAPI working={fetchWillSucceed} toggleAPI={toggleAPI} />
    </Fragment>
  );
};

export default ContactFormContainer;
