import React, { Fragment } from "react";
import styles from "./styles.module.css";
import {
  HandleSubmit,
  HandleChange,
  FormState,
} from "../../containers/ContactFormContainer/index";

interface Props {
  formState: FormState;
  handleSubmit: HandleSubmit;
  handleChange: HandleChange;
}

const ContactForm: React.FC<Props> = ({
  formState,
  handleSubmit,
  handleChange,
}) => {
  return (
    <Fragment>
      <header className={styles.header}>
        <h1>Contact Us</h1>
      </header>
      <section className={styles.section}>
        <form onSubmit={handleSubmit} className={styles.form}>
          <label htmlFor="name" className={styles.category}>
            Name
          </label>
          <input
            value={formState.name}
            onChange={handleChange}
            name="name"
            id="name"
            type="text"
            placeholder="Jane Doe"
            required
          />
          <label htmlFor="email" className={styles.category}>
            E-mail
          </label>
          <input
            value={formState.email}
            onChange={handleChange}
            name="email"
            id="email"
            type="email"
            placeholder="jane@doe.com"
            required
          />
          <div className={styles.genderDiv}>
            {/* <label htmlFor="gender">Gender</label> */}
            <p className={styles.category}>Gender</p>
            <div>
              <div>
                <input
                  type="radio"
                  name="gender"
                  id="male"
                  value="male"
                  onChange={handleChange}
                  checked={formState.gender === "male"}
                  required
                />
                <label htmlFor="male">Male</label>
              </div>
              <div>
                <input
                  type="radio"
                  name="gender"
                  id="female"
                  value="female"
                  onChange={handleChange}
                  checked={formState.gender === "female"}
                  required
                />
                <label htmlFor="female">Female</label>
              </div>
              <div>
                <input
                  type="radio"
                  name="gender"
                  id="other"
                  value="other"
                  onChange={handleChange}
                  checked={formState.gender === "other"}
                  required
                />
                <label htmlFor="other">Other</label>
              </div>
            </div>
          </div>
          <label htmlFor="message" className={styles.category}>
            How can we help?
          </label>
          <textarea
            value={formState.message}
            onChange={handleChange}
            name="message"
            id="message"
            rows={7}
            placeholder="Write your message here"
            required
          ></textarea>
          <button type="submit">Submit</button>
        </form>
      </section>
    </Fragment>
  );
};

export default ContactForm;
