import React from "react";
import styles from "./styles.module.css";

interface Props {
  working: boolean;
  toggleAPI: () => void;
}

const ToggleAPI: React.FC<Props> = ({ working, toggleAPI }) => {
  return (
    <div className={styles.div}>
      {working ? (
        <p className={styles.working}>working</p>
      ) : (
        <p className={styles.broken}>broken</p>
      )}
      <button onClick={toggleAPI}>Toggle API Status</button>
    </div>
  );
};

export default ToggleAPI;
