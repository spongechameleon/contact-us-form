import React, { Fragment } from "react";
import { FetchStatus } from "../../containers/ContactFormContainer/index";
import styles from "./styles.module.css";
import { css } from "@emotion/core";
import BarLoader from "react-spinners/BarLoader";

interface Props {
  fetchStatus: FetchStatus;
}

const FetchMsg: React.FC<Props> = ({ fetchStatus: { status, errorCode } }) => {
  const override = css`
    display: block;
    margin: 1.8em auto 0;
  `;

  return (
    <Fragment>
      {/* Loading */}
      {status === "loading" && (
        <BarLoader
          css={override}
          height={6}
          width={200}
          color={"#3bc6a0"}
          loading={true}
        />
      )}
      {/* Success */}
      {status === "success" && (
        <div className={`${styles.msgBanner} ${styles.success}`}>
          <h1>
            Success! We have received your message and will be in touch soon.
          </h1>
        </div>
      )}
      {/* Error */}
      {status === "error" && (
        <div className={`${styles.msgBanner} ${styles.error}`}>
          <h1>Uh oh, something went wrong. Please try again.</h1>
          {errorCode && errorCode !== -1 && <p>HTTP Error: {errorCode}</p>}
          {errorCode && errorCode === -1 && <p>Network Connection Error</p>}
        </div>
      )}
    </Fragment>
  );
};

export default FetchMsg;
