# 'Contact Us' Form

[Live site](https://spongechameleon-contact-us.netlify.app/)

## How to run and test

Clone repo

```
git clone https://gitlab.com/spongechameleon/contact-us-form
```

Install dependencies

```
yarn
```

Run application

```
yarn start
```

## External Libraries Used

[react-spinners](https://www.npmjs.com/package/react-spinners) for the loading bar animation after a network request is initiated

## Design methodology

UI/UX:

- Consistent visual path down the page:

  - "Prompt"

  - "Input"

- Mobile-friendly radio buttons (horizontal instead of vertical)

State Management:

- Container component pattern used to centralize relevant state in one place (in the `ContactFormContainer` component)

## Bugs & Next Steps

- Radio buttons should be styled to match the rest of the form theme
